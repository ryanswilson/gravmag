import pytest
import gravmag as gm
import numpy as np

# bulge mass reported as 8.9*10^9 +-.89
def bm_test_base(shift=1, md=10, npts=300):
    rs = np.linspace(0, md, npts)
    b_prof = gm.bulge_profile(rs)
    bm1 = np.sum(2 * np.pi * b_prof * rs) / npts * md
    print(np.format_float_scientific(bm1))
    assert bm1 == pytest.approx(8.9 * 10 ** 9, rel=0.1)


def test_bulge_mass():
    bm_test_base()
    bm_test_base(shift=2)
    bm_test_base(md=20, npts=100)


# total stellar mass reported 5*10^10 roughly (4.6-6.4)
def total_test_base(shift=1, md=20, npts=100):
    rs = np.linspace(0, md, npts)
    prof = gm.total_profile(rs)
    bm1 = np.sum(2 * np.pi * prof * rs) / npts * md
    print(np.format_float_scientific(bm1))
    assert bm1 == pytest.approx(5 * 10 ** 10, rel=0.3)


def test_total_mass():
    total_test_base()
    total_test_base(shift=2)
    total_test_base(md=15, npts=300)


# TODO write test for speed from no B

# if __name__=="__main__":
