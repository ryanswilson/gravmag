fn main() {
    println!("Hello, world!");
}

fn linspace(a: f64, b: f64, n: usize) -> Vec<f64> {
    let mut v = Vec::new();

    let delta = (b - a) / ((n as f64) - 1.0);
    let mut w = a;
    let mut i = 0;
    while i < n {
        v.push(w);
        w += delta;
        i += 1;
    }
    return v;
}

pub struct RefiningIntegrator {
    points: Vec<(f64, f64)>,
}

impl RefiningIntegrator {
    fn integrate_inner<F>(self, f: F, x0: f64, x1: f64, f0: f64, f1: f64, eps: f64) -> f64
    where
        F: Fn(f64) -> f64 + Copy,
    {
        let dx = x1 - x0;
        let xm = (x0 + x1) / 2.0;
        let fm = self.run(f, xm);
        let est1 = dx * (f0 + f1) / 2.0;
        let est2 = dx * (f0 + 2.0 * fm + f1) / 4.0;
        let dif = (est1 - est2).abs();
        if dif < eps {
            return est2;
        }
        return self.integrate_inner(f, x0, xm, f0, fm, eps / 2.0)
            + self.integrate_inner(f, xm, x1, fm, f1, eps / 2.0);
    }

    fn integrate<F>(mut self, f: F, (a, b): (f64, f64), eps: f64, n: usize) -> f64
    where
        F: Fn(f64) -> f64 + Copy,
    {
        self.points = Vec::new();
        let xs = linspace(a, b, n + 1);
        let mut acc = 0.0;
        let mut pf = self.run(f, xs[0]);

        let mut i = 0;
        while i < n {
            let wf = self.run(f, xs[i + 1]);
            acc += self.integrate_inner(f, xs[i], xs[i + 1], pf, wf, eps / (n as f64));
            pf = wf;

            i += 1;
        }
        return acc;
    }

    fn run<F>(mut self, f: F, x: f64) -> f64
    where
        F: Fn(f64) -> f64 + Copy,
    {
        let rv = f(x);
        self.points.push((rv, x));
        return rv;
    }
}
