## Gravito-Magnetism as Dark Matter

There was a [paper](https://link.springer.com/article/10.1140/epjc/s10052-021-08967-3) published in 2/2021 that argued that dark matter could be explained by gravito-magnetic effects from the rotation of the galaxy. This claim was not obviously wrong to me (maybe should have been in retrospect) so I created this project to test if this explanation worked for the milky way. My analysis showed that the gravito-magnetic effect is around 3 orders of magnitude too small to explain the effects of dark matter.
