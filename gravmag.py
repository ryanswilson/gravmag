import numpy as np
from scipy.special import ellipk, ellipe
import matplotlib.pyplot as plt

G = 4.3 * 10 ** -6
# using kpc/msun * (km/s)**2


class ref_int:
    def __init__(self):
        self.points = []
        self.dat = None

    def run(self, f, x):
        rv = f(x)
        self.points.append([x, rv])
        return rv

    def integrate(self, f, x0, x1, eps, N=10):
        self.points = []
        xs = np.linspace(x0, x1, N + 1)
        acc = 0
        pf = self.run(f, xs[0])
        for i in range(N):
            wf = self.run(f, xs[i + 1])
            acc += self.integrate_inner(f, xs[i], xs[i + 1], pf, wf, eps / float(N))
            pf = wf
        return acc

    def integrate_inner(self, f, x0, x1, f0, f2, eps):
        dx = x1 - x0
        xm = (x0 + x1) / 2.0
        f1 = self.run(f, xm)
        est1 = dx * (f0 + f2) / 2.0
        est2 = dx * (f0 + 2 * f1 + f2) / 4.0
        dif = np.abs(est1 - est2)
        if dif < eps:
            return est2

        return self.integrate_inner(
            f, x0, xm, f0, f1, eps / 2.0
        ) + self.integrate_inner(f, xm, x1, f1, f2, eps / 2.0)

    def scatter(self):
        xs = [p[0] for p in self.points]
        ys = [p[1] for p in self.points]
        plt.scatter(xs, ys)


w = ref_int()


def bulge_dens(r, z, q=0.5, alpha=1.8, rc=2.1, ro=0.075, rho_b=95.6, shift=1.0):
    # rho is density /pc^3 leaving as is because other densities are per pc^2
    rc = rc * shift
    ro = ro * shift
    rho_b /= shift ** 3
    rp = np.sqrt(r ** 2 + (z / q) ** 2)
    return rho_b * np.exp(-((rp / rc) ** 2)) * (1 + rp / ro) ** (-alpha)


def int_bulge(r, shift=1.0):
    if r > 5.0 * shift:
        return 0
    wf = lambda z: bulge_dens(r, z, shift=shift)
    # returned value should be in msun/kpc
    return w.integrate(wf, -5 * shift, 5 * shift, 10 ** -3) * 10 ** 9


def bulge_profile(rs, shift=1.0):
    return np.asarray([int_bulge(r, shift) for r in rs])


def disk_profile(r):
    rdn = 2.9
    sdn = 816

    rdk = 3.31
    sdk = 210
    afp = lambda r: sdn * np.exp(-r / rdn) + sdk * np.exp(-r / rdk)
    return afp(r)


def total_profile(rs, shift=1.0):
    bulges = bulge_profile(rs, shift)
    return bulges + disk_profile(rs) * 10 ** 6


def mpf_f(r, rs, mprof, arr=False):
    # this function exists to smooth out discontinuities from the integration going
    # to infinity
    wv = np.interp(r, rs, mprof)
    rl = rs[-1]
    if arr:
        wv[r > rl] = (mprof[-1] * (rl / (r + 10 ** -6)) ** 3.5)[r > rl]
        return wv
    if r > rl:
        return mprof[-1] * (rl / r) ** 3.5
    return wv


# total profile should be mstel/kpc^2
# bulge starts as msun/pc^3
# disk as msun/pc^2
# bulge mass reported as 8.9*10^9 +-.89
# total stellar mass reported 5*10^10 roughly (4.6-6.4)


m1p = lambda m: (2 - m - np.sqrt(1 - m) * 2) / m
m2p = lambda m: (2 - m + np.sqrt(1 - m) * 2) / m


def phi_integrand(m, r, mpf):

    ms1 = m1p(m)
    ms2 = m2p(m)
    mt1 = mpf(ms1 * r) * ms1 ** 1.5
    # print(mt1,"mt1")
    mt2 = mpf(ms2 * r) * ms2 ** 1.5
    # print(mt2)
    mterm = mt1 + mt2
    # print(mterm,"mterm")
    # mterm*=10**-6
    return ellipk(m) / 2 / np.sqrt(m * (1 - m)) * mterm * 4 * G * r


def calc_phi(rs, mpf, inteps=100, eps=10 ** -6, N=4):
    acc = []
    for i, r in enumerate(rs[1:]):
        w.dat = (i, r)
        wf = lambda m: phi_integrand(m, r, mpf)
        acc.append(w.integrate(wf, eps, 1 - eps, inteps, N=N))
    acc = [acc[0]] + acc
    return acc


def numgrad(dr, ys):
    wr = (np.roll(ys, -1) - np.roll(ys, 1)) / 2 / dr
    wr[0] = (ys[1] - ys[0]) / dr
    wr[-1] = (ys[-1] - ys[-2]) / dr
    return wr


# speed for solar system around 200km/s
def easy_phi(md, npts, inteps=10 ** 4, eps=10 ** -6, N=4, shift=1.0):
    rs = np.linspace(0, md, npts)
    mprof = total_profile(rs, shift)
    mpf = lambda r: mpf_f(r, rs, mprof)
    phi = calc_phi(rs, mpf, inteps, eps, N)
    return phi


def calc_dphi(md, npts, inteps=10 ** 3, eps=10 ** -6, N=4, shift=1.0, mprof=None):
    rs = np.linspace(0, md, npts)
    if mprof is None:
        mprof = total_profile(rs, shift)
    mpf = lambda r: mpf_f(r, rs, mprof)
    phi = calc_phi(rs, mpf, inteps, eps, N)

    return numgrad(rs[1], phi)


def vf_phi(rs, dphis):
    return (-dphis * rs) ** 0.5


# TODO figure out proper units on G/c^2
# final B field in km/s/kpc
Gc = 4.78 * 10 ** -17


def s_star(m, beta):
    first = beta * ellipk(m) / m
    second = (m + beta * m - beta) / m / (1 - m) * ellipe(m)
    return first + second


def b_integrand_nom(pr, ir, jpf, eps):
    # point r, integrating r
    beta = pr / (ir + eps)
    mt = 4 * ir * pr / (ir + pr) ** 2
    m = mt - eps * mt ** 4 + eps * (1 - mt) ** 4
    return -4 * Gc * ir ** 2 * jpf(ir) * (pr + ir) ** -3 * s_star(m, beta)


ti = ref_int()
sigma = 0.3


def full_theta_integrand(theta, r, R):
    a2 = r ** 2 + r ** 2 - 2 * r * R * np.cos(theta)
    num = np.sqrt(2 / np.pi) * (R - r * np.cos(theta))
    # this is a (very good) approximation for the z integral as derived by
    # "josemptotics"
    return num / a2 * (a2 + sigma ** 2 * np.pi / 2) ** (-0.5)


def b_integrand_full(pr, ir, jpf, eps):
    wf = lambda theta: full_theta_integrand(theta, pr, ir)
    return -ti.integrate(wf, 0, np.pi, eps, N=8) * 2 * Gc * ir * jpf(ir)


def calc_b_full(rs, jpf, md, inteps=0.01, eps=10 ** -6, N=4):
    acc = []
    for i, r in enumerate(rs[1:]):
        w.dat = (i, r)
        wf = lambda R: b_integrand_full(r, R, jpf, eps=eps)
        acc.append(w.integrate(wf, 0, md, inteps, N=N))
    acc = [acc[0]] + acc
    return acc


def calc_b_nom(rs, jpf, md, inteps=100, eps=10 ** -6, N=4):
    acc = []
    for i, r in enumerate(rs[1:]):
        w.dat = (i, r)
        wf = lambda R: b_integrand_nom(r, R, jpf, eps=eps)
        acc.append(w.integrate(wf, 0, md, inteps, N=N))
    acc = [acc[0]] + acc
    return acc


mdict = {"nom": calc_b_nom, "full": calc_b_full}


class easy_b:
    def __init__(self, md, npts, shift=1.0):
        self.md = md
        self.npts = npts
        self.rs = np.linspace(0, md, npts)
        self.b_hist = []
        self.v_hist = []
        self.b = np.zeros(npts)
        self.v = np.zeros(npts)
        self.mprof = total_profile(self.rs, shift)
        self.mpf = lambda r, arr: mpf_f(r, self.rs, self.mprof, arr)
        self.dphi = calc_dphi(md, npts, mprof=self.mprof)

    # TODO use better fall off function for jpf, reasonable argument for 1/2 or 5/2
    def jpf(self, r, arr=False):
        wm = self.mpf(r, arr)
        wv = np.interp(r, self.rs, self.v)
        rl = self.rs[-1]
        if arr:
            wv[r > rl] = (self.v[-1] * (rl / (r + 10 ** -6)) ** 3.5)[r > rl]
            return wv * wm
        if r > rl:
            wv = self.v[-1] * (rl / r) ** 3.5
        return wv * wm

    def compute_v(self):
        rb = self.rs * self.b
        gterm = -4 * self.rs * self.dphi
        self.v = (rb + np.sqrt(rb ** 2 + gterm)) / 2
        self.v_hist.append(self.v)

    def compute_b(self, inteps=10 ** -1, eps=10 ** -6, N=5, mode="full"):
        self.b = mdict[mode](
            self.rs, self.jpf, 2 * self.md, inteps=inteps, N=N, eps=eps
        )
        self.b_hist.append(self.b)
